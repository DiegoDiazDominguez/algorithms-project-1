package algorithms_design_project_1;

import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

public class CommandLineArguments {
	
	public static Options buildCommandLineOptions(){

		Options options = new Options();
		Option inputFile = Option.builder("i")
				.longOpt("input-file")
				.desc("input file with the segments")
				.hasArg().argName("inputFile")
				.required().build();
		Option logFile = Option.builder("l")
				.longOpt("log-file")
				.desc("log file to store statistics")
				.hasArg().argName("logFile")
				.required().build();
		Option b = Option.builder("B")
				.longOpt("block-size")
				.desc("Size of the block in bytes")
				.hasArg().argName("blockSize")
				.required().build();
		Option m = Option.builder("M")
				.longOpt("RAM-size")
				.desc("Size of the RAM in bytes")
				.hasArg().argName("ramSize")
				.required().build();
		Option h = Option.builder("h").longOpt("help").build();
		
		options.addOption(inputFile);
		options.addOption(logFile);
		options.addOption(b);
		options.addOption(m);
		options.addOption(h);

		return options;
	}
}
