package algorithms_design_project_1;

public class Counter {

	private static Counter readInstance = null;
	private static Counter writeInstance = null;
	private static Counter interInstance = null;
	private long counter;

	protected Counter() {
		counter = 0;
	}

	public static Counter getReadInstance() {
		if(readInstance == null) {
			readInstance = new Counter();
		}
		return readInstance;
	}

	public static Counter getWriteInstance() {
		if(writeInstance == null) {
			writeInstance = new Counter();
		}
		return writeInstance;
	}

	public static Counter getIntersectInstance() {
		if(interInstance == null) {
			interInstance = new Counter();
		}
		return interInstance;
	}

	public void count(){
		counter +=1;
	}
	
	public long getCounterValue(){
		return counter;
	}
}
