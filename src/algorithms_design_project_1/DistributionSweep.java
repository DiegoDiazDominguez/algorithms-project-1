package algorithms_design_project_1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class DistributionSweep {

	private String inputFile;
	private String sortedByX;
	private String sortedByY;
	private double M; // RAM size
	private double B; // page size
	private long m; // number of pages in RAM
	private static String HORIZONTAL = "0";
	private static String VERTICAL_START = "1";
	private static String VERTICAL_END = "-1";
	
	public DistributionSweep(String file, long B, long M){
		inputFile = file;
		this.M = M;
		this.B = B; 
		m = (int)Math.floor((float)(this.M/this.B));
		
		sortedByX = this.inputFile+"_sortedByX.txt";
		sortedByY = this.inputFile+"_sortedByY.txt";
	}
	
	private int [] defineSlabs(String file, double nLines, long m) throws IOException{

		String [] buffer = null;
		HugeFileReader xSegReader = new HugeFileReader(new File(file), this.B);
		int nSlabs = (int) Math.ceil(nLines / m); 
		int cont = 1;
		int posNext = 0;
		int [] positions = new int[nSlabs];
		int[] slabs = new int[positions.length + 1];
		int start=0;
		int end=0;
		double fraction = m/nLines;
		double accFrac = fraction;
		double currPos;
		boolean first = true;
		boolean hasData = true;
		
		xSegReader.open();
		if(fraction>1){
			fraction = 1;
			accFrac = 1;
		}
		
		for(int i=0;i < positions.length;i++){
			positions[i] = (int) Math.floor(nLines*accFrac);
			accFrac += fraction;
		}

		currPos = positions[posNext];
		
		while(hasData){
			buffer = xSegReader.getDataChunk((int)this.m);
			if(buffer == null){
				hasData = false;
			}else{
				for(int i=0;i<buffer.length;i++){
					int value = Integer.parseInt(buffer[i].split(",")[0]);
					int value2 = Integer.parseInt(buffer[i].split(",")[2]);

					if(first){
						start =  value;
						end = value2;
						first = false;
					}
					if(cont==currPos){
						slabs[posNext+1] = value;
						posNext += 1;
						if(posNext < positions.length){
							currPos = positions[posNext];
						}
					}
					cont+=1;
					if(value2>end){
						end = value2;
					}
				}
			}
		}
		
		slabs[0] = start;
		slabs[slabs.length-1] = end;

		xSegReader.close();
		return slabs;
	}
	
	private void sortDoubleY() throws IOException{

		String[] buffer;
		String[] fields;
		String tmp;
		String interFileName = this.inputFile+"_inter_file.txt";
		File interFile = new File(interFileName);
		HugeFileReader reader = new HugeFileReader(new File(this.inputFile), this.B);
		HugeFileWriter writer = new HugeFileWriter(interFile);
		boolean hasData = true;
		ExternalMergeSort emsY;
		ArrayList<String> tmpBuffer = new ArrayList<String>();

		// generate a copy of the input file
		reader.open();
		writer.open();
		while(hasData){
			buffer = reader.getDataChunk((int)this.m);
			if(buffer == null){
				hasData = false;
			}else{
				for(int i=0;i<buffer.length;i++){
					fields = buffer[i].split(",");
					if(fields[1].equals(fields[3])){
						buffer[i] = buffer[i]+","+HORIZONTAL;
					}else{
						buffer[i] = buffer[i]+","+VERTICAL_START;
					}
				}
				writer.writeData(String.join("\n", buffer)+"\n");
			}
		}
		reader.close();
		// append a new copy of the input file, with the Y coordinates flipped
		reader = new HugeFileReader(new File(this.inputFile), this.B);
		reader.open();
		hasData = true;
		// generate a second copy for the input file
		while(hasData){
			buffer = reader.getDataChunk((int)this.m);
			if(buffer == null){
				hasData = false;
			}else{
				// flip Y coordinates and put a flag
				for(int i=0;i<buffer.length;i++){
					fields = buffer[i].split(",");
					if(!fields[1].equals(fields[3])){
						tmp = fields[1];
						fields[1] = fields[3];
						fields[3] = tmp;
						tmpBuffer.add(String.join(",", fields)+","+VERTICAL_END);
					}
				}
				writer.writeData(String.join("\n", tmpBuffer)+"\n");
				tmpBuffer.clear();
			}
		}
		
		reader.close();
		writer.close();

		// sort by Y coordinate the output file
		emsY = new ExternalMergeSort(interFileName, this.B, this.M, 1, sortedByY);
		emsY.sortExternal();
		interFile.delete();
		
	}
	
	private void distSweepAlgorithm() throws IOException{
		ExternalMergeSort emsX = new ExternalMergeSort(this.inputFile, this.B, this.M, 0, sortedByX);
		emsX.sortExternal();
		sortDoubleY();
		System.gc();
		recursiveSweep(sortedByY, sortedByX, emsX.getLinesNumber(), "0_0");
	}
	
	private void recursiveSweep(String listY, String listX, double nLines, String iterNumber) throws IOException{

		File sweepList = new File(listY);
		String [] sweepBuffer;
		String[] fields;
		String segmentType;
		boolean hasData = true;
		int nSlabs = (int)Math.ceil(nLines/this.m);
		int slabIndex;
		int maxCapacity = (int) (Math.floor(this.M/(4*5*nSlabs*4))-16);
		int b = (int) Math.min(maxCapacity, nLines/nSlabs); 
		int buffSize = b*5;
		int [] nextIterBuffer = null;
		int [] activeSegmentsBuffer = new int[buffSize*nSlabs];
		int [] asp = new int[nSlabs];
		int [] nip = new int[nSlabs];
		int [] slabs;
		HugeFileWriter[] nextIterFiles = null;

		// we keep iterating until the data fits the memory
		if(sweepList.length() < this.M){
			// generate just one slab with all the segments (base case)
			slabs = defineSlabs(listX, nLines, this.m);
			defineSlabs(listX, nLines, (long)nLines);
		}else{
			// generate m slabs
			slabs = defineSlabs(listX, nLines, this.m);
			nextIterBuffer = new int[buffSize*nSlabs];
			nextIterFiles = new HugeFileWriter[slabs.length-1];
		}
		
		HugeFileWriter[] activeSegmentsWriters = new HugeFileWriter[slabs.length-1];
		HugeFileReader[] activeSegmentsReaders = new HugeFileReader[slabs.length-1];
		
		// setup files used in the analysis
		setupFiles(activeSegmentsReaders, activeSegmentsWriters, nextIterFiles, iterNumber);
		
		// read the segments's Y coordinates
		HugeFileReader hfr = new HugeFileReader(sweepList, this.B);
		hfr.open();
		while(hasData){
			sweepBuffer = hfr.getDataChunk(1); 
			if(sweepBuffer==null){
				hasData = false;
			}else{
				for(int i=0; i<sweepBuffer.length;i++){
					fields = sweepBuffer[i].split(",");
					segmentType = fields[fields.length-1];

					if(segmentType.equals(VERTICAL_START)){
						
						
						// get which slab this segment belongs to
						slabIndex = getSlabIndex(Integer.parseInt(fields[0]), slabs);
						int buffStartPos = buffSize*slabIndex;
						int aslCurrPos = buffStartPos + asp[slabIndex];
						int nipCurrPos = buffStartPos + nip[slabIndex];
						int xPos = Integer.parseInt(fields[0]);
						int start = Integer.parseInt(fields[1]); 
						int end = Integer.parseInt(fields[3]);
						
						if(asp[slabIndex]==buffSize){
							dumpData(activeSegmentsWriters[slabIndex], activeSegmentsBuffer, buffStartPos, aslCurrPos-1);
							asp[slabIndex] = 0;
							aslCurrPos = buffStartPos;
						}
						
						// save the segment in the list of active segments for the slab
						activeSegmentsBuffer[aslCurrPos] = xPos;
						activeSegmentsBuffer[aslCurrPos + 1] = start;
						activeSegmentsBuffer[aslCurrPos + 2] = xPos;
						activeSegmentsBuffer[aslCurrPos + 3] = end;
						activeSegmentsBuffer[aslCurrPos + 4] = 1; 

						asp[slabIndex] += 5;
						
						if(nextIterFiles!=null){

							if(nip[slabIndex]==buffSize){
								dumpData(nextIterFiles[slabIndex], nextIterBuffer, buffStartPos, buffSize*slabIndex+nip[slabIndex]-1);
								nip[slabIndex] = 0;
								nipCurrPos = buffStartPos;
							}

							// save segments for the next iteration
							nextIterBuffer[nipCurrPos] = xPos;
							nextIterBuffer[nipCurrPos + 1] = start;
							nextIterBuffer[nipCurrPos + 2] = xPos;
							nextIterBuffer[nipCurrPos + 3] = end;
							nextIterBuffer[nipCurrPos + 4] = 1;

							nip[slabIndex] += 5;
						}	
						
					}else if(segmentType.equals(HORIZONTAL)){
						
						int start = Integer.parseInt(fields[0]); 
						int end = Integer.parseInt(fields[2]);
						int yPos = Integer.parseInt(fields[1]);

						// get (i+1)-(j-1) slabs intersecting this horizontal segment
						int [] slabsOnTheSegment = getBorderSlabs(start, end, slabs);
						int buffStartPos;
						int buffCurrPos;
						int slabEnd;
						int slabStart;

						if(nextIterFiles!=null){
							// save segment for i slab 
							if(slabsOnTheSegment[0]-1 >= 0){
								slabIndex = (slabsOnTheSegment[0] - 1);
								buffStartPos = buffSize*slabIndex;
								buffCurrPos = buffStartPos + nip[slabIndex];
							
								if(nip[slabIndex]==buffSize){
									dumpData(nextIterFiles[slabIndex], nextIterBuffer, buffStartPos, buffCurrPos-1);
									nip[slabIndex] = 0;
									buffCurrPos = buffStartPos;
								}
							
								nextIterBuffer[buffCurrPos] = start;
								nextIterBuffer[buffCurrPos + 1] = yPos;
								nextIterBuffer[buffCurrPos + 2] = end;
								nextIterBuffer[buffCurrPos + 3] = yPos;
								nextIterBuffer[buffCurrPos + 4] = 0;

								nip[slabIndex] += 5;
							}

							// save segment for j slab
							if(slabsOnTheSegment[slabsOnTheSegment.length-1]<slabs.length-2){
								slabIndex = slabsOnTheSegment[slabsOnTheSegment.length-1] + 1;
								buffStartPos = buffSize*slabIndex;
								buffCurrPos = buffStartPos + nip[slabIndex];
							
								if(nip[slabIndex]==buffSize){
									dumpData(nextIterFiles[slabIndex], nextIterBuffer, buffStartPos, buffCurrPos-1);
									nip[slabIndex] = 0;
									buffCurrPos = buffStartPos;
								}
							
								nextIterBuffer[buffCurrPos] = start;
								nextIterBuffer[buffCurrPos + 1] = yPos;
								nextIterBuffer[buffCurrPos + 2] = end;
								nextIterBuffer[buffCurrPos + 3] = yPos;
								nextIterBuffer[buffCurrPos + 4] = 0;
							
								nip[slabIndex] += 5;
							}
						}
						
						// search for intersections
						for(int j=slabsOnTheSegment[0]; j < slabsOnTheSegment[1]; j++){
							slabStart = buffSize*j;
							slabEnd = slabStart+buffSize-1;
							segmentIntersect(start, end, yPos,
											 activeSegmentsBuffer, slabStart, slabEnd,
											 activeSegmentsWriters, activeSegmentsReaders,
											 asp, j, buffSize);
						}
						
					}
				}
			}
		}
		hfr.close();

		// dump the remaining segments for the next iteration
		if(nextIterFiles!=null){
			for(int i=0; i<nextIterFiles.length;i++){
				int buffStartPos = buffSize * i;
				int buffCurrPos = buffStartPos + nip[i];
				dumpData(nextIterFiles[i], nextIterBuffer, buffStartPos, buffCurrPos-1);
			}
		}
		
		// clean temporal files
		for(int i=0; i<activeSegmentsWriters.length;i++){
			activeSegmentsWriters[i].getFile().delete();
			if(!activeSegmentsWriters[i].isClosed()){
				System.out.println(activeSegmentsWriters[i].isClosed());
				activeSegmentsWriters[i].close();
			}
			activeSegmentsWriters[i] = null;
			if(!activeSegmentsReaders[i].isClosed()){
				activeSegmentsReaders[i].close();
			}
			activeSegmentsReaders[i] = null;
		}
		
		activeSegmentsBuffer = null;
		asp = null;
		nip = null;
		slabs = null;

		// remove the list of this iteration to avoid collisions
		sweepList.delete();
		new File(listX).delete();

		System.gc();

		// next iteration in the sweep
		if(nextIterFiles!=null){
			String nextIterNumber = Integer.toString(Integer.parseInt(iterNumber.split("_")[0].trim())+1);
			for(int i=0;i<nextIterFiles.length;i++){
				String nextIter = nextIterFiles[i].getFile().getPath();
				String nextIterSortedByX = nextIter+"_sortedByX.txt";
				ExternalMergeSort emsX = new ExternalMergeSort(nextIter, this.B, this.M, 0, nextIterSortedByX);
				emsX.sortExternal();
				recursiveSweep(nextIter, nextIterSortedByX, emsX.getLinesNumber(), nextIterNumber+"_"+Integer.toString(i));
			}
		}
	}
	
	
	private void setupFiles(HugeFileReader[] readers, HugeFileWriter[] writers, HugeFileWriter[] nextIter, String iterNumber) throws IOException{
		
		// for each slab create a temporal file to store active segments
		// and create other file to store the segments for the next iteration
		for(int i=0;i<readers.length;i++){
			// path to y-sorted segments
			Path ySortedPath = Paths.get(sortedByY);

			// to write in the list of active segments
			String activeSegments = inputFile+"_active_segments_slab_"+Integer.toString(i)+"_"+iterNumber;

			String activeSegmentsPath = ySortedPath.getParent().resolve(activeSegments).toString();
			File activeSegmentsFile = new File(activeSegmentsPath);

			if(activeSegmentsFile.exists()){
				activeSegmentsFile.delete();
			}

			activeSegmentsFile.createNewFile();

			writers[i] = new HugeFileWriter(activeSegmentsFile);
			
			// to write in the list of segments for the next iteration
			if(nextIter!=null){
				String nextIterName = inputFile+"_Ylist_"+Integer.toString(i)+"_iter_"+iterNumber;

				String nextIterPath = ySortedPath.getParent().resolve(nextIterName).toString();
				File nextIterFile = new File(nextIterPath);

				if(nextIterFile.exists()){
					nextIterFile.delete();
				}
				nextIterFile.createNewFile();
				nextIter[i] = new HugeFileWriter(nextIterFile);
			}

			// to read in the list of active segments
			readers[i] = new HugeFileReader(activeSegmentsFile, this.B);
		}
	}
	
	private void dumpData(HugeFileWriter hfw, int[] segments, int start, int end) throws IOException{
		String res="";
		for(int i=start;i<=end;i=i+5){
			res += Integer.toString(segments[i])+","+
			Integer.toString(segments[i+1])+","+
			Integer.toString(segments[i+2])+","+
			Integer.toString(segments[i+3])+","+
			Integer.toString(segments[i+4])+"\n";
			segments[i] = 0;
			segments[i+1] = 0;
			segments[i+2] = 0;
			segments[i+3] = 0;
			segments[i+4] = 0;
		}
		hfw.open();
		hfw.writeData(res);
		hfw.close();
	}

	private void segmentIntersect(long xStart, long xEnd, long yPos, int[] slabsBuffer, int start, int end, HugeFileWriter[] activeSegmentsWriters, HugeFileReader[] activeSegmentsReaders, int[] positions, int sIndex, int bufferCapacity) throws IOException{

		
		int[] tmpBuffer = new int[bufferCapacity]; 
		int bufferCont = 0;
		File slabDumpFile = null;
		File slabTmpDumpFile = null;
		HugeFileWriter hfw = null;
		
		if(activeSegmentsReaders[sIndex].getFileSize()!=0){
			boolean hasData = true;
			String[] buffer;
			String[] fields;
			slabDumpFile = activeSegmentsWriters[sIndex].getFile();
			slabTmpDumpFile = new File(slabDumpFile.getPath()+"_tmp.txt");
			hfw = new HugeFileWriter(slabTmpDumpFile);

			if(activeSegmentsReaders[sIndex].isClosed()){
				try{
					activeSegmentsReaders[sIndex].open();
				}catch(IOException e){
					// close readers if there are too many open streams
					if(sIndex==0){
						activeSegmentsReaders[sIndex+1].close();
					}else if(sIndex<=activeSegmentsReaders.length-1){
						activeSegmentsReaders[sIndex-1].close();
					}
				}finally{
					activeSegmentsReaders[sIndex].open();
				}
			}

			// check the active elements stored in the slab's file. Inactive segments are discarded.
			while(hasData){
				buffer = activeSegmentsReaders[sIndex].getDataChunk(1);
				if(buffer == null){
					hasData = false;
				}else{
					for(int i=0;i<buffer.length;i++){

						fields = buffer[i].split(",");
						int y2 = Integer.parseInt(fields[3]);

						if(y2 >= yPos){

							int x1 = Integer.parseInt(fields[0]);
							int y1 = Integer.parseInt(fields[1]);
							int x2 = x1;
							int h = 1;
							

							if(xStart <= x1 && x1 <= xEnd){
								Counter.getIntersectInstance().count();
								//System.out.println("interseccion: ("+xStart+" "+yPos+" "+xEnd+" "+yPos+") ("+buffer[i]+")");
							}

							if(bufferCont==bufferCapacity){
								dumpData(hfw, tmpBuffer, 0 , bufferCapacity-1);
								bufferCont = 0;
							}

							tmpBuffer[bufferCont] = x1;
							bufferCont += 1;
							tmpBuffer[bufferCont] = y1;
							bufferCont += 1;
							tmpBuffer[bufferCont] = x2;
							bufferCont += 1;
							tmpBuffer[bufferCont] = y2;
							bufferCont += 1;
							tmpBuffer[bufferCont] = h;
							bufferCont += 1;
						
						}
					}
				}
			}
			activeSegmentsReaders[sIndex].reset();
		}

		// check which segments in the buffer's slab are active.
		// Report intersections and discard inactive ones. 
		for(int i=start; i<=end; i=i+5){
			if(slabsBuffer[i+3] >= yPos){
				if(xStart <= slabsBuffer[i] && slabsBuffer[i] <= xEnd){
					Counter.getIntersectInstance().count();
					//System.out.println("interseccion: ("+xStart+" "+yPos+" "+xEnd+" "+yPos+") ("+slabsBuffer[i]+" "+slabsBuffer[i+1]+" "+slabsBuffer[i+2]+" "+slabsBuffer[i+3]+")");
				}
				if(bufferCont==bufferCapacity){
					if(hfw==null){
						slabDumpFile = activeSegmentsWriters[sIndex].getFile();
						slabTmpDumpFile = new File(slabDumpFile.getPath()+"_tmp.txt");
						hfw = new HugeFileWriter(slabTmpDumpFile);
					}
					dumpData(hfw, tmpBuffer, 0 , bufferCapacity-1);
					bufferCont = 0;
				}

				tmpBuffer[bufferCont] = slabsBuffer[i];
				bufferCont += 1;
				tmpBuffer[bufferCont] = slabsBuffer[i+1];
				bufferCont += 1;
				tmpBuffer[bufferCont] = slabsBuffer[i+2];
				bufferCont += 1;
				tmpBuffer[bufferCont] = slabsBuffer[i+3];
				bufferCont += 1;
				tmpBuffer[bufferCont] = slabsBuffer[i+4];
				bufferCont += 1;
				
			}
			slabsBuffer[i] = 0;
			slabsBuffer[i+1] = 0;
			slabsBuffer[i+2] = 0;
			slabsBuffer[i+3] = 0;
			slabsBuffer[i+4] = 0;
		}

		int pos = start;
		for(int i=0;i<bufferCont;i++){
			slabsBuffer[pos] = tmpBuffer[i];
			pos += 1;
		}
		
		if(hfw!=null){
			hfw.renameFile(slabDumpFile);
			activeSegmentsWriters[sIndex] = hfw;
		}

		positions[sIndex] = bufferCont;
	}
	
	private int getSlabIndex(int pos, int[] slabs){
		if(pos < slabs[0]){
			return 0;
		}else if(pos>slabs[slabs.length-1]){
			return slabs.length-1;
		}else{
			for(int i=0; i< slabs.length-1; i++){
				if(slabs[i]<= pos && pos <slabs[i+1]){
					return i;
				}
			}
		}
		return 0;
	}
	
	private int[] getBorderSlabs(int start, int end, int[] slabs){
		int [] borders = new int[2];
		int nSlabs = 0;
		int slabIndexStart=0;
		int slabIndexEnd=0;
		if(start < slabs[0]){
			slabIndexStart = 0;
		}
		
		if(end > slabs[slabs.length-1]){
			slabIndexEnd = slabs.length-1;
		}

		for(int i=0;i<slabs.length-1;i++){
			if(slabs[i]<= start && start <slabs[i+1]){
				slabIndexStart = i;
			}
			if(slabs[i]<= end && end <slabs[i+1]){
				slabIndexEnd = i;
			}
		}
		nSlabs = slabIndexEnd - slabIndexStart;
		if(nSlabs>2){
			if(start > slabs[slabIndexStart]){
				borders[0] = slabIndexStart+1;
			}else{
				borders[0] = slabIndexStart;
			}
			if(end > slabs[slabIndexEnd]){
				borders[1] = slabIndexEnd-1;
			}else{
				borders[1] = slabIndexEnd;
			}
		}else{
			if(nSlabs<=2){
				borders[0] = slabIndexStart;
				borders[1] = slabIndexStart+1;
			}
		}
		return borders;
	}
	
	public static void main(String[] args) throws IOException{

		CommandLineParser parser = new DefaultParser();
		Options options = CommandLineArguments.buildCommandLineOptions();
		HelpFormatter formatter = new HelpFormatter();
		FileWriter fw = null;
		BufferedWriter bw = null;

		try{
			//args = new String[]{"-i /Users/diegodiaz/Documents/workspace/algorithms_design_project_1/tests/0.25_1048576_normal.txt", "-B 4096", "-M 4194304", "-l pepe"};
			CommandLine line = parser.parse(options, args);
		    String inputFile = line.getOptionValue("input-file").trim();
		    String logFile = line.getOptionValue("log-file").trim();
		    File f=null;
		    Long B = Long.parseLong(line.getOptionValue("block-size").trim());
		    Long M = Long.parseLong(line.getOptionValue("RAM-size").trim());
		    double mLog = Math.log(M)/Math.log(2);
		    mLog = mLog - Math.floor(mLog);
		    double bLog = Math.log(B)/Math.log(2);
		    bLog = bLog - Math.floor(bLog);
		    f = new File(inputFile);

		    if(mLog != 0 || bLog != 0){
		    	throw new ParseException(" M and B values must be powers of 2");
		    }

		    if(B>=M){
		    	throw new ParseException(" RAM size must be greater than block size");
		    }
		    
		    if(!f.exists()){
		    	throw new ParseException(" input file doesn't exist");
		    }else{
		    	inputFile = f.getAbsolutePath();
		    }
		    
		    //
		    DistributionSweep ds = new DistributionSweep(inputFile, 4096, 4194304);
			long startTime = System.nanoTime();
			ds.distSweepAlgorithm();
			long difference = System.nanoTime() - startTime;
			final long hr = TimeUnit.NANOSECONDS.toHours(difference);
	        final long min = TimeUnit.NANOSECONDS.toMinutes(difference - TimeUnit.HOURS.toMillis(hr));
	        final long sec = TimeUnit.NANOSECONDS.toSeconds(difference - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min));
	        final long ms = TimeUnit.NANOSECONDS.toMillis(difference - TimeUnit.HOURS.toMillis(hr) - TimeUnit.MINUTES.toMillis(min) - TimeUnit.SECONDS.toMillis(sec));
	        fw = new FileWriter(logFile);
			bw = new BufferedWriter(fw);
	        bw.write("Elapsed time: "+String.format("%02d:%02d:%02d.%03d", hr, min, sec, ms)+"\n");
			bw.write("Number of writes to disk: "+Counter.getReadInstance().getCounterValue()+"\n");
			bw.write("Number of reads to disk: "+Counter.getWriteInstance().getCounterValue()+"\n");
			bw.write("Number of intersections detected: "+Counter.getIntersectInstance().getCounterValue()+"\n");
			bw.close();

		}catch(ParseException exp){
			formatter.printHelp( "DistributionSweep", options );
			System.out.println( "Unexpected exception: " + exp.getMessage());
		}
	}	
}
