package algorithms_design_project_1;

public class MergeSort {

	public String [] array;
	private String [] tmpArray;
	private int[] intArray;
	private int[] tmpIntArray;
	private int[] segOrient;
	private int[] tmpSegOrient;
	private static final int HORIZONTAL = 0;
	private static final int VERTICAL = 1;
	
	public MergeSort(String[] array){
		this.array = array;
		this.intArray = new int[array.length];
		this.segOrient = new int[array.length];
		this.tmpSegOrient = new int[array.length];
		this.tmpIntArray = new int[array.length];
		this.tmpArray = new String[array.length];
	}
	
	public void sort(int column){
		String [] tmp;
		for(int i=0;i<array.length;i++){
			 tmp = array[i].split(",");
			 intArray[i] = Integer.parseInt(tmp[column]);
			 if(tmp[0].equals(tmp[2])){
				 segOrient[i] = VERTICAL;
			 }else{
				 if(tmp[1].equals(tmp[3])){
					 segOrient[i] = HORIZONTAL;
				 }
			 }
			 //System.out.println(array[i]+" "+segOrient[i]);
		}
		this.doMergeSort(0, array.length - 1, column);
	}
	
	public String[] getList(){
		return this.array;
	}
	
	private void doMergeSort(int start, int end, int column){

		int middle = start + (int) Math.floor((end - start + 1)/2);
		int i=start;
		int j=middle;
		int k = start;
		
		if(start!=end){

			doMergeSort(start, middle-1, column);
			doMergeSort(middle, end, column);

			while(i<middle && j<=end){
				boolean flag = true;
				// select which is greater
				if(intArray[i]<intArray[j]){
					flag = true;
				}else if(intArray[i]>intArray[j]){
					flag = false;
				}else if(intArray[i]==intArray[j]){
					if(this.segOrient[i]==VERTICAL && this.segOrient[j] == HORIZONTAL){
						flag = true;
					}else{
						if(this.segOrient[i]==HORIZONTAL && this.segOrient[j] == VERTICAL){
							flag = false;
						}
					}
					//System.out.println(array[i]+" "+array[j]+" "+flag+" "+segOrient[i]+" "+segOrient[j]);
				}
				
				if(flag){
					tmpArray[k] = array[i];
					tmpIntArray[k] = intArray[i];
					tmpSegOrient[k] = segOrient[i];
					k+=1;
					i+=1;
				}else{
					tmpArray[k] = array[j];
					tmpIntArray[k] = intArray[j];
					tmpSegOrient[k] = segOrient[j];
					k+=1;
					j+=1;
				}
			}
			
			while(i<middle){
				tmpArray[k] = array[i];
				tmpIntArray[k] = intArray[i];
				tmpSegOrient[k] = segOrient[i];
				i +=1;
				k+=1;
			}
			while(j<=end){
				tmpArray[k] = array[j];
				tmpIntArray[k] = intArray[j];
				tmpSegOrient[k] = segOrient[j];
				j +=1;
				k+=1;
			}
			
			for(int c=start;c<k;c++){
				array[c] = tmpArray[c];
				intArray[c] = tmpIntArray[c];
				segOrient[c] = tmpSegOrient[c];

			}
		}
	}
	
	public static void main(String[] args){
		
		String[] elements = {
				"2,2,12,2",
				"5,1,5,11",
				"3,4,3,15",
				"3,10,3,15",
				"11,2,11,9",
				"11,5,11,14",
				"3,13,8,13",
				"5,13,5,15",
				"3,8,6,8",
				"13,14,13,15",
				"15,6,15,11",
				"6,6,6,14",
				"6,14,13,14",
				"8,8,8,14",
				"10,8,10,13",
				"8,8,10,8",
				"5,11,11,11",
				"8,11,15,11",
				"3,4,7,4",
				"7,4,10,4"
		};

		MergeSort ms = new MergeSort(elements);
		ms.sort(1);
		String[] sorted = ms.getList();
		for(int i=0;i<sorted.length;i++){
			System.out.println(sorted[i]);
		}
	}
}
