package algorithms_design_project_1;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

public class HugeFileReader{
	
	private final double pageSize;
	private MappedByteBuffer MBBuffer = null;
	private FileChannel fileChannel = null;
	private long red = 0;
	private long read = 0;
	private byte[] buffer;
	private String rest;
	private boolean fileConsumed;
	private boolean closed;
	private File file;
	private RandomAccessFile hfr;

	public HugeFileReader(File file, double blockSize) throws IOException{
		pageSize = blockSize;
        fileConsumed = false;
        closed = true;
		this.file = file;
        rest = "";
	}
	
	private int readPages(byte[] pagesBuffer, int nPages) throws IOException{
		double nPagesSize = nPages * this.pageSize;
		double bufferSize = 0;
		
		if(pagesBuffer.length != nPagesSize){
			throw new IOException("size of the buffer is different to nPages * pageSize");
		}
		
		// file is empty
		if(hfr.length()==0){
			return -1;
		}
		
		if(!this.fileConsumed){
			
			bufferSize = Math.min(this.MBBuffer.remaining(), nPagesSize);
			this.MBBuffer.get(pagesBuffer, 0, (int)bufferSize);
			Counter.getReadInstance().count();
			
			if(this.MBBuffer.remaining() == 0){
				this.red += this.read;
				if(this.red < this.fileChannel.size()){
					this.fillMBBuffer();
				}else{
					this.fileConsumed = true;
				}
			}
			return 0;
		}else{
			return -1;
		}
	}
	
	public void reset() throws IOException{
		read = 0;
		red = 0;
		fillMBBuffer();
        fileConsumed = false;
	}

	public boolean isClosed(){
		return closed;
	}
	
	public void open() throws IOException{
		if(!closed){
			close();
		}
		file = new File(file.getPath());
		hfr = new RandomAccessFile(file, "r");
		fileChannel = hfr.getChannel();
		reset();
        closed = false;
	}
	
	public long getFileSize() throws IOException{
		return file.length();
	}

	public void fillMBBuffer() throws IOException{
		this.read = Math.min(Integer.MAX_VALUE, this.fileChannel.size() - this.red);
		this.MBBuffer = this.fileChannel.map(FileChannel.MapMode.READ_ONLY, red, read);
	}
	
	public void close() throws IOException{
		hfr.close();
		fileChannel.close();
		closed = true;
	}
	
	public double getPageSize(){
		return this.pageSize;
	}
	
	public String[] getDataChunk(int nChunks) throws IOException{

		int nIter;
		String data;
		String lines[];
		String segments[];
		buffer = new byte[nChunks*(int)this.pageSize];
		int hasData;
		String endOfLine = "";
		String startOfLine = "";
		
		if(!closed){
			hasData = readPages(buffer, nChunks);

			// it means there is no more data in the buffer
			if(hasData == -1){
				return null;
			}
			data = (new String(buffer, "UTF-8"));
		
			if(data.endsWith("\n")){
				endOfLine = "\n";
			}
		
			if(data.startsWith("\n")){
				startOfLine = "\n";
			}else{
				startOfLine = "";
			}
		
			data = data.trim();
			data = rest+startOfLine+data;
			lines = data.split("\n");
		
			if(this.fileConsumed){
				segments = new String[lines.length];
				nIter = lines.length;
				rest = "";
			}else{
				segments = new String[lines.length-1];
				nIter = lines.length-1;
				rest = lines[lines.length-1]+endOfLine;
			}
			for(int i=0;i<nIter;i++){
				segments[i] = lines[i];
			}
			return segments;
		}else{
			throw new IOException("trying to read in a closed HugeFileReader");
		}
	}
	
	public String getLastLine() throws IOException{
		
        StringBuilder builder = new StringBuilder();
        long length = hfr.length();
        long oldOffset = hfr.getFilePointer();
        length--;
        hfr.seek(length);
        boolean firstRead = false;
        
        for(long seek = length; seek >= 0; --seek){
            hfr.seek(seek);
            char c = (char)hfr.read();
            builder.append(c);
            if(!firstRead){
            	firstRead = true;
            }else{
            	if(c=='\n'){
            		hfr.seek(oldOffset);
            		break;
            	}
            }
        }
        
        builder = builder.reverse();
        return builder.toString().replaceAll("\n", "");
	}

	public String getFirstLine() throws IOException{

        StringBuilder builder = new StringBuilder();
        long oldOffset = hfr.getFilePointer();
        hfr.seek(0);
        boolean firstRead = false;
        long fSize = hfr.length();
        
        for(long seek = 0; seek < fSize ; seek++){
            hfr.seek(seek);
            char c = (char)hfr.read();
            builder.append(c);
            if(!firstRead){
            	firstRead = true;
            }else{
            	if(c=='\n'){
            		hfr.seek(oldOffset);
            		break;
            	}
            }
        }
        
        return builder.toString().replaceAll("\n", "");
	}
}
