package algorithms_design_project_1;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


public class ExternalMergeSort {
	
	private ArrayList<String> runsFiles;
	private double M; // RAM size
	private double B; // page size
	private double N; // input size
	private int n; // number of pages of the input
	private int m; // number of pages in RAM
	private int column; // column to sort
	private int lineNumbers;
	private File inputFile;
	private int reductions;
	private static final int HORIZONTAL = 0;
	private static final int VERTICAL = 1;
	private String outputFile;
	
	
	public ExternalMergeSort(String unsortedFile, double B, double M, int column, String outputFile){
		this.inputFile = new File(unsortedFile);
		this.runsFiles = new ArrayList<String>();
		this.M = M;
		this.B = B; 
		this.N = this.inputFile.length(); 
		this.n = (int)Math.ceil((float)(N/B));
		this.m = (int)Math.floor((float)(this.M/B));
		this.column = column;
		this.reductions = 0;
		this.outputFile = outputFile;
	}
	
	public void sortExternal(){
		try {
			this.generateRuns();
			this.sortRuns();
			
			File oldfile = new File(this.runsFiles.get(0));
			File newfile = new File(this.outputFile);

			if(oldfile.renameTo(newfile)){
				//System.out.println(this.inputFile+" succesfully sorted");
			}else{
				System.out.println("There was a problem trying to generate the output file");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void generateRuns() throws IOException{
		
		int part=0;
		int redRun = 0;
		String runFile;
		String data;
		String[] run;
		File tmpFile;
		MergeSort ms;
		HugeFileReader reader = new HugeFileReader(this.inputFile, B);
		reader.open();

		// iterate while there are unread pages in the input file
		while(redRun < this.n){
			data = "";
			part+=1;
			runFile = inputFile+"_run_"+Integer.toString(part)+".txt";
			this.runsFiles.add(runFile);
			tmpFile = new File(runFile);

			if(tmpFile.exists()){
				tmpFile.delete();
			}			
			
			HugeFileWriter hfw = new HugeFileWriter(tmpFile);
			hfw.open();
			run = reader.getDataChunk(this.m);
			this.lineNumbers += run.length;
			ms = new MergeSort(run);
			ms.sort(this.column);
			data = String.join("\n", run)+"\n";
			hfw.writeData(data);
			hfw.close();
			redRun += this.m;
		}
		
		reader.close();
	}
	
	private void sortRuns() throws IOException{
		while(this.runsFiles.size()>1){
			reduceRuns();
		}
	}

	public int getLinesNumber(){
		return this.lineNumbers;
	}

	private void reduceRuns() throws IOException{

		// get the number of runs to be joined
		int nRuns = this.runsFiles.size();
		// number of sorts to reduce the number of runs by factor of m
		int nLoads = (int)Math.ceil((float)nRuns/this.m);
		// number of pages loaded per Run in a given sort
		int nPagesPerRun;
		// number of Runs loaded in a given sort
		int nRunsLoaded;
		int cont = 0;
		File tmpFile;

		// Runs obtained when the currents runs are joined 
		ArrayList<String> newRuns = new ArrayList<String>();

		// Load just one page per Run. We left one page for buffering purposes
		if(nRuns>=this.m){
			nPagesPerRun = 1;
			nRunsLoaded = this.m - 1;
		}else{
			// If the number of Runs is less than m-1, then we load several pages
			//of the current Runs 
			nPagesPerRun = (int) Math.floor((float)((this.m - 1) / nRuns));
			nRunsLoaded = nRuns;
		}
		
		// Join m-1 Runs into one new Run.
		for(int i=0;i<nLoads;i++){

			HugeFileReader [] runsHandlers = new HugeFileReader[nRunsLoaded];

			for(int j=0;j<nRunsLoaded;j++){
				tmpFile = new File(this.runsFiles.get(cont));
				cont += 1;
				runsHandlers[j] = new HugeFileReader(tmpFile, B);
				runsHandlers[j].open();
			}

			// name for the new Run
			String name = this.inputFile.getParent()+"/run_red_"+
					Integer.toString(this.reductions)+"_"+Integer.toString(i)+".txt";

			newRuns.add(name);
			// join runs
			this.reduceRuns(runsHandlers, nPagesPerRun, name);
		}
		
		for(int i=0;i<this.runsFiles.size();i++){
			Path file = Paths.get(this.runsFiles.get(i));
			try {
			    Files.delete(file);
			} catch (NoSuchFileException x) {
			    System.err.format("%s: no such" + " file or directory%n", file);
			} catch (DirectoryNotEmptyException x) {
			    System.err.format("%s not empty%n", file);
			} catch (IOException x) {
			    System.err.println(x);
			}
			
		}
		
		this.runsFiles = newRuns;
		this.reductions += 1;
	}
	
	private void reduceRuns(HugeFileReader handlers[], int nPagesPerRun, String outputFile) throws IOException{
		/*
		 * buffer = [[[1,2,3,4],[3,1,4,5],[1,2,3,4],[2,3,4,1]]
		 * 			[[2,4,5,1],[2,4,5,1],[4,5,1,3],[1,2,3,4]]
		 *          [[2,3,4,1],[1,5,6,7],[7,8,1,2],[7,2,1,4]]]
		 * Each row represents a different Run. Each row
		 * is a chunk of sorted elements of the Run. For each Run,
		 * there is a pointer indicating the position in the row
		 * where the sorting is going so far. If all
		 * elements of a Run were sorted, then the buffer is refilled
		 * with the next chunk and the pointer is moved to the first position
		 * in the row
		 */
		int min;
		int bytesStored = 0;
		int nEmpty = 0; 
		int[] positions = new int[handlers.length];
		int[][] intValues = new int[handlers.length][];
		int [][] segmentOr = new int[handlers.length][];
		boolean keepSorting = true;
		String[][] buffers = new String[handlers.length][];
		ArrayList<String> saveBuffer = new ArrayList<String>();
		HugeFileWriter hfw = new HugeFileWriter(new File(outputFile));
		hfw.open();
		String [] tmp;
		
		// Load a Chunk of data for each Run
		for(int i=0;i<handlers.length;i++){
			try {
				buffers[i] = handlers[i].getDataChunk(nPagesPerRun);
				intValues[i] = new int[buffers[i].length];
				segmentOr[i] = new int[buffers[i].length];

				// Transform to Integer just the values in the column to be analyzed
				for(int j=0; j<buffers[i].length;j++){
					tmp = buffers[i][j].split(",");
					
					intValues[i][j] = Integer.parseInt(tmp[column]);
					
					if(tmp[0] == tmp[1]){
						segmentOr[i][j] = VERTICAL;
					}else{
						if(tmp[1] == tmp[2]){
							segmentOr[i][j] = HORIZONTAL;
						}
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		// iterate over the Runs until sort all the values
		while(keepSorting){

			// get the Run with the minimum value in the column position
			min = getMin(intValues, positions, segmentOr);

			// check if the buffer is completed. If so, then write 
			//the information and clear the buffer
			if(bytesStored > this.B){
				tmp = new String[saveBuffer.size()];
				hfw.writeData(String.join("\n", saveBuffer.toArray(tmp))+"\n");
				saveBuffer.clear();
				bytesStored = 0;
			}
			
			saveBuffer.add(buffers[min][positions[min]]);
			bytesStored += buffers[min][positions[min]].getBytes().length;
			
			// move the next element in the Run with the minimum value
			positions[min] += 1;
			//refill the buffer if all the elements in the buffer of min run were sorted
			if(positions[min] == buffers[min].length){

				buffers[min] = handlers[min].getDataChunk(nPagesPerRun);

				// if this run has no more data, then it is marked as empty
				if(buffers[min] == null){
					nEmpty +=1;
					intValues[min] = null;
					positions[min] = -1;
				}else{
					// new data is loaded
					intValues[min] = new int[buffers[min].length];
					segmentOr[min] = new int[buffers[min].length];

					for(int j=0;j<buffers[min].length;j++){
						String[] tmp1 = buffers[min][j].split(",");
						intValues[min][j] = Integer.parseInt(tmp1[column]);

						if(tmp1[0] == tmp1[1]){
							segmentOr[min][j] = VERTICAL;
						}else{
							if(tmp1[1] == tmp1[2]){
								segmentOr[min][j] = HORIZONTAL;
							}
						}
					}
					positions[min] = 0;
				}
			}

			// If all runs were consumed, then we stop the iteration
			if(nEmpty==handlers.length){
				keepSorting = false;
				// write the to file the remaining data
				tmp = new String[saveBuffer.size()];
				hfw.writeData(String.join("\n", saveBuffer.toArray(tmp))+"\n");
			}
		}
		
		for(int i=0;i<handlers.length;i++){
			handlers[i].close();
		}

		hfw.close();
	}
	
	private int getMin(int[][] elements, int[] currPos, int[][] segmentsOr){

		int min = -1;
		int minIndex = 0;
		int cont = 0;
		boolean changeMin;
		
		for(int i=0;i<elements.length; i++){
			
			changeMin = false;
			//some items were already sorted and stored. The first non-empty value is the first min
			if(elements[i]!=null){
				if(cont==0){
					min = elements[i][currPos[i]];
					minIndex = i;
					cont += 1;
				}else{
					if(elements[i][currPos[i]] < min){
						changeMin = true;
					}else if(elements[i][currPos[i]] == min){
						//System.out.println(segmentsOr[i].length+" "+currPos[i]);
						if(segmentsOr[i][currPos[i]] == VERTICAL && segmentsOr[minIndex][currPos[minIndex]] == HORIZONTAL){
							changeMin = true;
						}
					}
				}
			}
			
			if(changeMin){
				min = elements[i][currPos[i]];
				minIndex = i;
			}
		}
		return minIndex;
	}
	
	/*public static void main(String[] args){
		String filePath = "/Users/diegodiaz/Documents/workspace/algorithms_design_project_1/tests/test_data.txt";
		ExternalMergeSort ems = new ExternalMergeSort(filePath, 1024, 4194304, 1, "asdas"); 
		try {
			ems.generateRuns();
			ems.sortRuns();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}*/
}
