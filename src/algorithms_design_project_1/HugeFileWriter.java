package algorithms_design_project_1;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;


public class HugeFileWriter{

	private FileChannel rwChannel;
	private ByteBuffer wrBuf;
	private int pos;
	private File file;
	private RandomAccessFile raf; 
	private boolean isClosed;
	private long writtenData;
	
	public HugeFileWriter(File file) throws IOException{
		this.file = file;
    	pos = 0;
    	isClosed = true;
    	writtenData = 0;
	}
	
	public void writeData(String data) throws IOException{
		if(!isClosed){
			wrBuf = rwChannel.map(FileChannel.MapMode.READ_WRITE, pos, data.length());
			byte [] bytesToWrite = data.getBytes();
			wrBuf.put(bytesToWrite);
			Counter.getWriteInstance().count();
			pos += data.length();
			writtenData += bytesToWrite.length;
		}else{
			throw new IOException("trying to write in a closed stream");
		}
	}
	
	public void close() throws IOException{
    	rwChannel.close();
    	raf.close();
    	isClosed = true;
	}
	
	public boolean isClosed(){
		return isClosed;
	}
	
	public void open() throws IOException{
		raf = new RandomAccessFile(file, "rw");
		rwChannel = raf.getChannel();
		isClosed = false;
	}
	
	public File getFile(){
		return this.file;
	}
	
	public int getPosition(){
		return pos;
	}
	
	public void setPosition(int pos){
		this.pos = (int) pos;
	}
	
	public void renameFile(File file){
		this.file.renameTo(file);
		this.file = file;
	}
	
	public long getBytesWritten(){
		return writtenData;
	}
}
